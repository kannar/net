# Copyright 2009, 2011 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ apache.service ] ]

export_exlib_phases pkg_pretend pkg_setup src_prepare src_compile src_install

myexparam apache_modules[]

exparam -v APACHE_MODULES apache_modules[@]

PV2=$(ever range 1-2)

SUMMARY="An open-source HTTP server"
DESCRIPTION="
The Apache HTTP Server Project is an effort to develop and maintain an
open-source HTTP server for modern operating systems including UNIX and Windows
NT. The goal of this project is to provide a secure, efficient and extensible
server that provides HTTP services in sync with the current HTTP standards.
Apache has been the most popular web server on the Internet since April 1996.
"
HOMEPAGE="https://httpd.apache.org"
DOWNLOADS="mirror://apache/httpd/httpd-${PV}.tar.bz2"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="
    https://www.apache.org/dist/httpd/CHANGES_${PV}  [[ note = [ ChangeLog for ${PV} ] ]]
    https://www.apache.org/dist/httpd/CHANGES_${PV2} [[ note = [ Complete ChangeLog for ${PV2} ] ]]
"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/${PV2}/"
UPSTREAM_RELEASE_NOTES="https://httpd.apache.org/docs/${PV2}/new_features_${PV2//./_}.html"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="
    apache_modules: ${APACHE_MODULES[*]}
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# configure.in checks net-misc/rsync, not sure why
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/apr:1[>=1.2.0]
        dev-libs/apr-util:1[>=1.2.0]
        dev-libs/pcre[>=6.0]
        group/apache
        user/apache
        || (
            net-www/links
            net-www/elinks
            net-www/lynx
        ) [[ description = [ Used by apachectl ] ]]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

WORK=${WORKBASE}/httpd-${PV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-layout=Exherbo
    --enable-so

    --datadir=/usr/share/apache2
    --includedir=/usr/$(exhost --target)/include/apache2
    --localstatedir=/var
    --sysconfdir=/etc/apache2

    --without-included-apr
    # Use external pcre, --without-pcre enables an internal copy
    --with-pcre
    --with-ssl

    --with-suexec-caller=apache
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "${APACHE_MODULES[@]/#/apache_modules:}"
    "apache_modules:suexec suexec-capabilities"
)

apache_pkg_pretend() {
    if [[ -f "${ROOT}"/etc/tmpfiles.d/${PN}.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/${PN}.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/${PN}.conf and can be safely removed after upgrade"
        ewarn "if you did not make any changes to it."
    fi
}

apache_pkg_setup() {
    exdirectory --allow /srv
}

apache_src_prepare() {
    default

    export PKGCONFIG=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)pkg-config

    add-exherbo-layout
}

apache_src_compile() {
    # apache uses TARGETS itself
    unset TARGETS

    default
}

apache_src_install() {
    # apache uses TARGETS itself
    unset TARGETS

    default

    keepdir /var/lib/dav /var/{cache,log}/apache2
    edo chown apache:apache "${IMAGE}"/var/lib/dav "${IMAGE}"/var/{cache,log}/apache2
    edo chmod 0755 "${IMAGE}"/var/lib/dav "${IMAGE}"/var/{cache,log}/apache2

    edo rm -r "${IMAGE}"/etc/apache2/original

    edo find "${IMAGE}"/usr/share/doc/${PNVR}/manual/ -type d -empty -delete

    install_systemd_files
    insinto /etc/conf.d
    doins "${FILES}"/systemd/apache.conf
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    newins "${FILES}"/systemd/apache.conf.tmpfiles apache.conf

    # Fix user/group.
    edo sed -i -e "s:^\(.* \)daemon$:\1 ${PN}:" "${IMAGE}"/etc/apache2/httpd.conf

    # Add modules.d config part.
    edo cat "${FILES}"/apache-modules.d.conf >> "${IMAGE}"/etc/apache2/httpd.conf
    keepdir /etc/apache2/modules.d

    # The init system takes care of creating this directory.
    edo rmdir "${IMAGE}"/run/{apache2,}
}

# Rather than trying to outsmart autoconf, we define our own layout, to get a
# proper filesystem layout for apache.
add-exherbo-layout() {
    cat <<EOF >> "${WORK}"/config.layout
# Exherbo layout
<Layout Exherbo>
    prefix:        /usr
    exec_prefix:   \${prefix}/$(exhost --target)
    bindir:        \${exec_prefix}/bin
    sbindir:       \${exec_prefix}/bin
    libdir:        \${exec_prefix}/lib
    libexecdir:    /usr/$(exhost --target)/libexec/apache2/modules
    mandir:        \${datadir}/man
    sysconfdir:    /etc+
    datadir:       \${datadir}
    installbuilddir: \${datadir}/build
    errordir:      \${datadir}/error
    iconsdir:      \${datadir}/icons
    htdocsdir:     /srv/www/htdocs
    # this autoconf version doesn't know --docdir
    manualdir:     /usr/share/doc/${PNVR}/manual
    cgidir:        /srv/www/cgi-bin
    includedir:    \${exec_prefix}/include+
    localstatedir: /var
    runtimedir:    /run/apache2
    logfiledir:    \${localstatedir}/log/apache2
    proxycachedir: \${localstatedir}/proxy
</Layout>
EOF
}

