# Copyright 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.gz ]

export_exlib_phases src_compile src_test src_install

SUMMARY="C library to run an HTTP server as part of another application"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    curl [[ description = [ Whether to use libcurl for testing ] ]]
    doc
    ssl
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        ssl? ( dev-libs/gnutls[>=3.5] )
    test:
        curl? ( net-misc/curl[>=7.21.6][providers:gnutls] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bauth
    --enable-dauth
    --enable-epoll
    --enable-httpupgrade
    --enable-itc=eventfd
    --enable-messages
    --enable-nls
    --enable-poll
    --enable-postprocessor
    --enable-sendfile
    --disable-asserts
    --disable-examples
    --disable-experimental
    --disable-static
    --with-threads=posix
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'ssl gnutls' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( curl doc 'ssl https' )

# tests using curl fail to run in parallel
DEFAULT_SRC_TEST_PARAMS=( -j1 )

libmicrohttpd_src_compile() {
    default

    if exhost --is-native -q && option doc; then
        emake -C doc/doxygen full
    fi
}

libmicrohttpd_src_install() {
    default

    if exhost --is-native -q && option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r doc/doxygen/html/*
    fi
}

libmicrohttpd_src_test() {
    # tests fail with sydbox enabled when running curl tests
    option curl && esandbox disable_net
    esandbox allow_net --connect "inet:127.0.0.1@1090"
    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "inet:0.0.0.0@1080-1090"
    # tests fail with recent curl (last checked: 0.9.57)
    option !curl && default
    esandbox disallow_net "inet:0.0.0.0@1080-1090"
    esandbox disallow_net "inet:0.0.0.0@0"
    esandbox disallow_net --connect "inet:127.0.0.1@1090"
    option curl && esandbox enable_net
}

